package main.java.com.sittiipol.lab13;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class HelloMyName extends JFrame {
    JLabel lblname ;
    JLabel lblHello;
    JTextField txtName;
    JButton btnHello;
    public HelloMyName(){
        super("Hello My Name");
        lblname  = new JLabel("Name: ");
        lblname.setBounds(10,10,50,20);
        lblname.setHorizontalAlignment(JLabel.RIGHT);

        txtName = new JTextField();
        txtName.setBounds(70,10,200,20);

        btnHello = new JButton("Hello");
        btnHello.setBounds(30,40,250,20);
        btnHello.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) { 
                String myName = txtName.getText();
                lblHello.setText("Hello "+ myName);
            }
        });

        lblHello = new JLabel("Hello My Name");
        lblHello.setBounds(30,70,250,20);
        lblHello.setHorizontalAlignment(JLabel.CENTER);

        this.add(lblHello);
        this.add(lblname);
        this.add(txtName);
        this.add(btnHello);

        this.setLayout(null);
        this.setSize(400,300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);

    }
    public static void main(String[] args) {
        HelloMyName frame = new HelloMyName();
    }
}
