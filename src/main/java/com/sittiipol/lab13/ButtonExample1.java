package main.java.com.sittiipol.lab13;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ButtonExample1 extends JFrame {
    JButton button;
    JButton clearButton;
    JTextField text ;
    public ButtonExample1() {
        super("Button Example");
        text = new JTextField();
        text.setBounds(50,50,150,20);
        button = new JButton("Click here");
        button.setBounds(50, 100, 95, 30);
        button.setIcon(new ImageIcon("welcome.png"));
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Click Button");
                text.setText("Welcome to Burpha");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.setBounds(165,100,95,30);
        clearButton.setIcon(new ImageIcon("clear.png"));
    
        clearButton.addActionListener(new ActionListener() {
            
            @Override
            public void actionPerformed(ActionEvent e){
                text.setText("");
            }
        });
        this.add(text);
        this.add(clearButton);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 400);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

    public static void main(String[] args) {
        ButtonExample1 frame = new ButtonExample1();
    }
}
